const { filesystem } = require("gluegun")

const deepTree = async (path, elioGroup, loadConfig) => {
  let deep = new Array()
  let pathTree = filesystem.inspectTree(path)
  let packageFile = pathTree.children.find(
    child => child.name === "package.json"
  )
  if (packageFile) {
    let kleioConfig = loadConfig("kleio", path)
    if (kleioConfig.hasOwnProperty("branciaos")) {
      deep.push({
        path: path,
        elioName: pathTree.name,
        elioGroup: elioGroup,
      })
    }
    for (let childPath of pathTree.children.filter(cP => cP.type === "dir")) {
      let deepPath = filesystem.path(path, childPath.name)
      let tree = await deepTree(deepPath, pathTree.name, loadConfig)
      deep = deep.concat(tree)
    }
  }
  return deep
}


module.exports = toolbox => {
  toolbox.branciaos = async () => {
    const {
      config: { loadConfig },
    } = toolbox
    let branciaos = []
    if (!filesystem.exists("/home/tim/.config/kleio/.kleiorc")) {
      throw "You need `~/.config/kleio/kleiorc.yaml`"
    }
    const kleioConfig = await loadConfig("kleio", "/home/tim/.config/kleio/")
    for (let repo of kleioConfig.defaults.optin) {
      let kleioOptinRoot = filesystem.path(kleioConfig.defaults.devRoot, repo)
      let kleioOptinBranciaos = await deepTree(kleioOptinRoot, "", loadConfig)
      branciaos = branciaos.concat(kleioOptinBranciaos)
    }
    return branciaos
  }
}
