![](https://elioway.gitlab.io/elioangels/branciao/elio-branciao-logo.png)

> Branciao, **the elioWay**

# branciao

Starter pack for an elioThing app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [branciao Documentation](https://elioway.gitlab.io/elioangels/branciao/)

## Installing

- [Installing branciao](https://elioway.gitlab.io/elioangels/branciao/installing.html)

## Requirements

- [elioangels Prerequisites](https://elioway.gitlab.io/elioangels/installing.html)

## Getting To Know Yeoman

- Yeoman has a heart of gold.
- Yeoman is a person with feelings and opinions, but is very easy to work with.
- Yeoman can be too opinionated at times but is easily convinced not to be.
- Feel free to [learn more about Yeoman](http://yeoman.io/).

## Seeing is Believing

```
You're seeing it.
```

- [elioangels Quickstart](https://elioway.gitlab.io/elioangels/quickstart.html)
- [branciao Quickstart](https://elioway.gitlab.io/elioangels/branciao/quickstart.html)

# Credits

- [branciao Credits](https://elioway.gitlab.io/elioangels/branciao/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/elioangels/branciao/apple-touch-icon.png)
